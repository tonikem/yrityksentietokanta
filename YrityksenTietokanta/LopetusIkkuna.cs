﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace YrityksenTietokanta
{
    public partial class LopetusIkkuna : Form
    {
        public LopetusIkkuna()
        {
            InitializeComponent();
            this.FormBorderStyle = FormBorderStyle.FixedSingle;
        }

        private void takaisin_nappi_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void poistu_nappi_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }
    }
}
