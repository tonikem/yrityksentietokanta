﻿using System.Drawing;

namespace YrityksenTietokanta
{
    partial class LisaysIkkuna
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources =
                new System.ComponentModel.ComponentResourceManager(typeof(LisaysIkkuna));
            this.etunimi_input = new System.Windows.Forms.TextBox();
            this.etunimi = new System.Windows.Forms.Label();
            this.sukunimi_input = new System.Windows.Forms.TextBox();
            this.sukunimi = new System.Windows.Forms.Label();
            this.kutsumanimi_input = new System.Windows.Forms.TextBox();
            this.kutsumanimi = new System.Windows.Forms.Label();
            this.etunimet_huom = new System.Windows.Forms.Label();
            this.kutsumanimet_huom = new System.Windows.Forms.Label();
            this.henkilotunnus_input = new System.Windows.Forms.TextBox();
            this.henkilötunnus = new System.Windows.Forms.Label();
            this.lahiosoite_input = new System.Windows.Forms.TextBox();
            this.lahiosoite = new System.Windows.Forms.Label();
            this.postinumero_input = new System.Windows.Forms.TextBox();
            this.postinumero = new System.Windows.Forms.Label();
            this.postitoimipaikka_input = new System.Windows.Forms.TextBox();
            this.postitoimipaikka = new System.Windows.Forms.Label();
            this.hyvaksy_uusi_nappi = new System.Windows.Forms.Button();
            this.varoitus_teksti = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // etunimi_input
            // 
            this.etunimi_input.ForeColor = System.Drawing.SystemColors.WindowText;
            this.etunimi_input.Location = new System.Drawing.Point(132, 21);
            this.etunimi_input.Name = "etunimi_input";
            this.etunimi_input.Size = new System.Drawing.Size(180, 20);
            this.etunimi_input.TabIndex = 0;
            this.etunimi_input.TextChanged += new System.EventHandler(this.etunimi_input_TextChanged);
            // 
            // etunimi
            // 
            this.etunimi.AutoSize = true;
            this.etunimi.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F);
            this.etunimi.Location = new System.Drawing.Point(69, 20);
            this.etunimi.Name = "etunimi";
            this.etunimi.Size = new System.Drawing.Size(57, 18);
            this.etunimi.TabIndex = 1;
            this.etunimi.Text = "Etunimi";
            // 
            // sukunimi_input
            // 
            this.sukunimi_input.Location = new System.Drawing.Point(132, 68);
            this.sukunimi_input.Name = "sukunimi_input";
            this.sukunimi_input.Size = new System.Drawing.Size(180, 20);
            this.sukunimi_input.TabIndex = 1;
            this.sukunimi_input.TextChanged += new System.EventHandler(this.sukunimi_input_TextChanged);
            // 
            // sukunimi
            // 
            this.sukunimi.AutoSize = true;
            this.sukunimi.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F);
            this.sukunimi.Location = new System.Drawing.Point(57, 68);
            this.sukunimi.Name = "sukunimi";
            this.sukunimi.Size = new System.Drawing.Size(69, 18);
            this.sukunimi.TabIndex = 1;
            this.sukunimi.Text = "Sukunimi";
            // 
            // kutsumanimi_input
            // 
            this.kutsumanimi_input.ForeColor = System.Drawing.SystemColors.WindowText;
            this.kutsumanimi_input.Location = new System.Drawing.Point(132, 94);
            this.kutsumanimi_input.Name = "kutsumanimi_input";
            this.kutsumanimi_input.Size = new System.Drawing.Size(180, 20);
            this.kutsumanimi_input.TabIndex = 2;
            this.kutsumanimi_input.TextChanged += new System.EventHandler(this.kutsumanimi_input_TextChanged);
            // 
            // kutsumanimi
            // 
            this.kutsumanimi.AutoSize = true;
            this.kutsumanimi.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F);
            this.kutsumanimi.Location = new System.Drawing.Point(32, 93);
            this.kutsumanimi.Name = "kutsumanimi";
            this.kutsumanimi.Size = new System.Drawing.Size(94, 18);
            this.kutsumanimi.TabIndex = 1;
            this.kutsumanimi.Text = "Kutsumanimi";
            // 
            // etunimet_huom
            // 
            this.etunimet_huom.AutoSize = true;
            this.etunimet_huom.Location = new System.Drawing.Point(129, 44);
            this.etunimet_huom.Name = "etunimet_huom";
            this.etunimet_huom.Size = new System.Drawing.Size(122, 13);
            this.etunimet_huom.TabIndex = 2;
            this.etunimet_huom.Text = "Erota nimet välilyönneillä";
            // 
            // kutsumanimet_huom
            // 
            this.kutsumanimet_huom.AutoSize = true;
            this.kutsumanimet_huom.Location = new System.Drawing.Point(129, 117);
            this.kutsumanimet_huom.Name = "kutsumanimet_huom";
            this.kutsumanimet_huom.Size = new System.Drawing.Size(188, 13);
            this.kutsumanimet_huom.TabIndex = 2;
            this.kutsumanimet_huom.Text = "Täytä, jos etunimiä enemmän kuin yksi";
            // 
            // henkilotunnus_input
            // 
            this.henkilotunnus_input.ForeColor = System.Drawing.SystemColors.WindowText;
            this.henkilotunnus_input.Location = new System.Drawing.Point(132, 142);
            this.henkilotunnus_input.Name = "henkilotunnus_input";
            this.henkilotunnus_input.Size = new System.Drawing.Size(180, 20);
            this.henkilotunnus_input.TabIndex = 3;
            this.henkilotunnus_input.TextChanged += new System.EventHandler(this.henkilotunnus_input_TextChanged);
            // 
            // henkilötunnus
            // 
            this.henkilötunnus.AutoSize = true;
            this.henkilötunnus.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F);
            this.henkilötunnus.Location = new System.Drawing.Point(24, 142);
            this.henkilötunnus.Name = "henkilötunnus";
            this.henkilötunnus.Size = new System.Drawing.Size(102, 18);
            this.henkilötunnus.TabIndex = 1;
            this.henkilötunnus.Text = "Henkilötunnus";
            // 
            // lahiosoite_input
            // 
            this.lahiosoite_input.ForeColor = System.Drawing.SystemColors.WindowText;
            this.lahiosoite_input.Location = new System.Drawing.Point(132, 168);
            this.lahiosoite_input.Name = "lahiosoite_input";
            this.lahiosoite_input.Size = new System.Drawing.Size(180, 20);
            this.lahiosoite_input.TabIndex = 4;
            this.lahiosoite_input.TextChanged += new System.EventHandler(this.lahiosoite_input_TextChanged);
            // 
            // lahiosoite
            // 
            this.lahiosoite.AutoSize = true;
            this.lahiosoite.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F);
            this.lahiosoite.Location = new System.Drawing.Point(50, 167);
            this.lahiosoite.Name = "lahiosoite";
            this.lahiosoite.Size = new System.Drawing.Size(76, 18);
            this.lahiosoite.TabIndex = 1;
            this.lahiosoite.Text = "Lähiosoite";
            // 
            // postinumero_input
            // 
            this.postinumero_input.ForeColor = System.Drawing.SystemColors.WindowText;
            this.postinumero_input.Location = new System.Drawing.Point(132, 194);
            this.postinumero_input.Name = "postinumero_input";
            this.postinumero_input.Size = new System.Drawing.Size(180, 20);
            this.postinumero_input.TabIndex = 5;
            this.postinumero_input.TextChanged += new System.EventHandler(this.postinumero_input_TextChanged);
            // 
            // postinumero
            // 
            this.postinumero.AutoSize = true;
            this.postinumero.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F);
            this.postinumero.Location = new System.Drawing.Point(33, 194);
            this.postinumero.Name = "postinumero";
            this.postinumero.Size = new System.Drawing.Size(93, 18);
            this.postinumero.TabIndex = 1;
            this.postinumero.Text = "Postinumero";
            // 
            // postitoimipaikka_input
            // 
            this.postitoimipaikka_input.ForeColor = System.Drawing.SystemColors.WindowText;
            this.postitoimipaikka_input.Location = new System.Drawing.Point(132, 220);
            this.postitoimipaikka_input.Name = "postitoimipaikka_input";
            this.postitoimipaikka_input.Size = new System.Drawing.Size(180, 20);
            this.postitoimipaikka_input.TabIndex = 6;
            this.postitoimipaikka_input.TextChanged += new System.EventHandler(this.postitoimipaikka_input_TextChanged);
            // 
            // postitoimipaikka
            // 
            this.postitoimipaikka.AutoSize = true;
            this.postitoimipaikka.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F);
            this.postitoimipaikka.Location = new System.Drawing.Point(9, 219);
            this.postitoimipaikka.Name = "postitoimipaikka";
            this.postitoimipaikka.Size = new System.Drawing.Size(117, 18);
            this.postitoimipaikka.TabIndex = 1;
            this.postitoimipaikka.Text = "Postitoimipaikka";
            // 
            // hyvaksy_uusi_nappi
            // 
            this.hyvaksy_uusi_nappi.Location = new System.Drawing.Point(221, 256);
            this.hyvaksy_uusi_nappi.Name = "hyvaksy_uusi_nappi";
            this.hyvaksy_uusi_nappi.Size = new System.Drawing.Size(91, 25);
            this.hyvaksy_uusi_nappi.TabIndex = 7;
            this.hyvaksy_uusi_nappi.Text = "Hyväksy";
            this.hyvaksy_uusi_nappi.UseVisualStyleBackColor = true;
            this.hyvaksy_uusi_nappi.Click += new System.EventHandler(this.hyvaksy_uusi_nappi_Click);
            // 
            // varoitus_teksti
            // 
            this.varoitus_teksti.AutoSize = true;
            this.varoitus_teksti.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.varoitus_teksti.ForeColor = System.Drawing.Color.Red;
            this.varoitus_teksti.Location = new System.Drawing.Point(9, 247);
            this.varoitus_teksti.Name = "varoitus_teksti";
            this.varoitus_teksti.Size = new System.Drawing.Size(0, 17);
            this.varoitus_teksti.TabIndex = 6;
            // 
            // LisaysIkkuna
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(338, 295);
            this.Controls.Add(this.varoitus_teksti);
            this.Controls.Add(this.hyvaksy_uusi_nappi);
            this.Controls.Add(this.kutsumanimet_huom);
            this.Controls.Add(this.etunimet_huom);
            this.Controls.Add(this.postitoimipaikka);
            this.Controls.Add(this.postinumero);
            this.Controls.Add(this.lahiosoite);
            this.Controls.Add(this.henkilötunnus);
            this.Controls.Add(this.kutsumanimi);
            this.Controls.Add(this.postitoimipaikka_input);
            this.Controls.Add(this.sukunimi);
            this.Controls.Add(this.postinumero_input);
            this.Controls.Add(this.lahiosoite_input);
            this.Controls.Add(this.etunimi);
            this.Controls.Add(this.henkilotunnus_input);
            this.Controls.Add(this.kutsumanimi_input);
            this.Controls.Add(this.sukunimi_input);
            this.Controls.Add(this.etunimi_input);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "LisaysIkkuna";
            this.Text = "Lisää Uusi";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox etunimi_input;
        private System.Windows.Forms.Label etunimi;
        private System.Windows.Forms.TextBox sukunimi_input;
        private System.Windows.Forms.Label sukunimi;
        private System.Windows.Forms.TextBox kutsumanimi_input;
        private System.Windows.Forms.Label kutsumanimi;
        private System.Windows.Forms.Label etunimet_huom;
        private System.Windows.Forms.Label kutsumanimet_huom;
        private System.Windows.Forms.TextBox henkilotunnus_input;
        private System.Windows.Forms.Label henkilötunnus;
        private System.Windows.Forms.TextBox lahiosoite_input;
        private System.Windows.Forms.Label lahiosoite;
        private System.Windows.Forms.TextBox postinumero_input;
        private System.Windows.Forms.Label postinumero;
        private System.Windows.Forms.TextBox postitoimipaikka_input;
        private System.Windows.Forms.Label postitoimipaikka;
        private System.Windows.Forms.Button hyvaksy_uusi_nappi;
        private System.Windows.Forms.Label varoitus_teksti;
    }
}