﻿namespace YrityksenTietokanta
{
    partial class LopetusIkkuna
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(LopetusIkkuna));
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.poistu_nappi = new System.Windows.Forms.Button();
            this.takaisin_nappi = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.label1.Location = new System.Drawing.Point(62, 24);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(207, 20);
            this.label1.TabIndex = 0;
            this.label1.Text = "Haluatko varmasti lopettaa?";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F);
            this.label2.Location = new System.Drawing.Point(29, 44);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(267, 18);
            this.label2.TabIndex = 0;
            this.label2.Text = "Tallentamattomat tiedot saattavat hävitä";
            // 
            // poistu_nappi
            // 
            this.poistu_nappi.Location = new System.Drawing.Point(186, 83);
            this.poistu_nappi.Name = "poistu_nappi";
            this.poistu_nappi.Size = new System.Drawing.Size(110, 34);
            this.poistu_nappi.TabIndex = 1;
            this.poistu_nappi.Text = "Kyllä";
            this.poistu_nappi.UseVisualStyleBackColor = true;
            this.poistu_nappi.Click += new System.EventHandler(this.poistu_nappi_Click);
            // 
            // takaisin_nappi
            // 
            this.takaisin_nappi.Location = new System.Drawing.Point(32, 83);
            this.takaisin_nappi.Name = "takaisin_nappi";
            this.takaisin_nappi.Size = new System.Drawing.Size(110, 34);
            this.takaisin_nappi.TabIndex = 1;
            this.takaisin_nappi.Text = "Ei";
            this.takaisin_nappi.UseVisualStyleBackColor = true;
            this.takaisin_nappi.Click += new System.EventHandler(this.takaisin_nappi_Click);
            // 
            // LopetusIkkuna
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(327, 136);
            this.ControlBox = false;
            this.Controls.Add(this.takaisin_nappi);
            this.Controls.Add(this.poistu_nappi);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "LopetusIkkuna";
            this.Text = "Lopetus";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button poistu_nappi;
        private System.Windows.Forms.Button takaisin_nappi;
    }
}