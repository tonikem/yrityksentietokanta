﻿namespace YrityksenTietokanta
{
    partial class ToimisuhdeIkkuna
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ToimisuhdeIkkuna));
            this.toimisuhteikkuna_otsikko = new System.Windows.Forms.Label();
            this.henkilo_valinta = new System.Windows.Forms.ListBox();
            this.label1 = new System.Windows.Forms.Label();
            this.lisaa_nappi = new System.Windows.Forms.Button();
            this.alkamispaiva = new System.Windows.Forms.DateTimePicker();
            this.paattymispaiva = new System.Windows.Forms.DateTimePicker();
            this.toistaiseksi = new System.Windows.Forms.CheckBox();
            this.varoitus = new System.Windows.Forms.Label();
            this.nimike_input = new System.Windows.Forms.TextBox();
            this.nimike = new System.Windows.Forms.Label();
            this.yksikko = new System.Windows.Forms.Label();
            this.yksikko_input = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // toimisuhteikkuna_otsikko
            // 
            this.toimisuhteikkuna_otsikko.AutoSize = true;
            this.toimisuhteikkuna_otsikko.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F);
            this.toimisuhteikkuna_otsikko.Location = new System.Drawing.Point(22, 23);
            this.toimisuhteikkuna_otsikko.Name = "toimisuhteikkuna_otsikko";
            this.toimisuhteikkuna_otsikko.Size = new System.Drawing.Size(231, 18);
            this.toimisuhteikkuna_otsikko.TabIndex = 0;
            this.toimisuhteikkuna_otsikko.Text = "Lisää tästä toimisuhteet henkilöille";
            // 
            // henkilo_valinta
            // 
            this.henkilo_valinta.AllowDrop = true;
            this.henkilo_valinta.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.henkilo_valinta.FormattingEnabled = true;
            this.henkilo_valinta.ItemHeight = 16;
            this.henkilo_valinta.Location = new System.Drawing.Point(127, 55);
            this.henkilo_valinta.Name = "henkilo_valinta";
            this.henkilo_valinta.Size = new System.Drawing.Size(126, 20);
            this.henkilo_valinta.TabIndex = 1;
            this.henkilo_valinta.SelectedIndexChanged += new System.EventHandler(this.henkilo_valinta_SelectedIndexChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.label1.Location = new System.Drawing.Point(22, 55);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(103, 17);
            this.label1.TabIndex = 2;
            this.label1.Text = "Valitse henkilö:";
            // 
            // lisaa_nappi
            // 
            this.lisaa_nappi.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F);
            this.lisaa_nappi.Location = new System.Drawing.Point(178, 256);
            this.lisaa_nappi.Name = "lisaa_nappi";
            this.lisaa_nappi.Size = new System.Drawing.Size(75, 23);
            this.lisaa_nappi.TabIndex = 3;
            this.lisaa_nappi.Text = "Lisää";
            this.lisaa_nappi.UseVisualStyleBackColor = true;
            this.lisaa_nappi.Click += new System.EventHandler(this.lisaa_nappi_Click);
            // 
            // alkamispaiva
            // 
            this.alkamispaiva.Location = new System.Drawing.Point(25, 81);
            this.alkamispaiva.Name = "alkamispaiva";
            this.alkamispaiva.Size = new System.Drawing.Size(228, 20);
            this.alkamispaiva.TabIndex = 4;
            this.alkamispaiva.ValueChanged += new System.EventHandler(this.alkamispaiva_ValueChanged);
            // 
            // paattymispaiva
            // 
            this.paattymispaiva.Location = new System.Drawing.Point(25, 107);
            this.paattymispaiva.Name = "paattymispaiva";
            this.paattymispaiva.Size = new System.Drawing.Size(228, 20);
            this.paattymispaiva.TabIndex = 4;
            this.paattymispaiva.ValueChanged += new System.EventHandler(this.paattymispaiva_ValueChanged);
            // 
            // toistaiseksi
            // 
            this.toistaiseksi.AutoSize = true;
            this.toistaiseksi.Location = new System.Drawing.Point(25, 133);
            this.toistaiseksi.Name = "toistaiseksi";
            this.toistaiseksi.Size = new System.Drawing.Size(129, 17);
            this.toistaiseksi.TabIndex = 5;
            this.toistaiseksi.Text = "Toistaiseksi Voimassa";
            this.toistaiseksi.UseVisualStyleBackColor = true;
            this.toistaiseksi.CheckedChanged += new System.EventHandler(this.toistaiseksi_CheckedChanged);
            // 
            // varoitus
            // 
            this.varoitus.AutoSize = true;
            this.varoitus.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.varoitus.ForeColor = System.Drawing.Color.Red;
            this.varoitus.Location = new System.Drawing.Point(12, 256);
            this.varoitus.Name = "varoitus";
            this.varoitus.Size = new System.Drawing.Size(0, 15);
            this.varoitus.TabIndex = 6;
            // 
            // nimike_input
            // 
            this.nimike_input.Location = new System.Drawing.Point(25, 180);
            this.nimike_input.Name = "nimike_input";
            this.nimike_input.Size = new System.Drawing.Size(228, 20);
            this.nimike_input.TabIndex = 7;
            this.nimike_input.TextChanged += new System.EventHandler(this.nimike_input_TextChanged);
            // 
            // nimike
            // 
            this.nimike.AutoSize = true;
            this.nimike.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.nimike.Location = new System.Drawing.Point(22, 160);
            this.nimike.Name = "nimike";
            this.nimike.Size = new System.Drawing.Size(50, 17);
            this.nimike.TabIndex = 2;
            this.nimike.Text = "Nimike";
            // 
            // yksikko
            // 
            this.yksikko.AutoSize = true;
            this.yksikko.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.yksikko.Location = new System.Drawing.Point(22, 203);
            this.yksikko.Name = "yksikko";
            this.yksikko.Size = new System.Drawing.Size(56, 17);
            this.yksikko.TabIndex = 2;
            this.yksikko.Text = "Yksikkö";
            // 
            // yksikko_input
            // 
            this.yksikko_input.Location = new System.Drawing.Point(25, 223);
            this.yksikko_input.Name = "yksikko_input";
            this.yksikko_input.Size = new System.Drawing.Size(228, 20);
            this.yksikko_input.TabIndex = 7;
            this.yksikko_input.TextChanged += new System.EventHandler(this.yksikko_input_TextChanged);
            // 
            // ToimisuhdeIkkuna
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(274, 290);
            this.Controls.Add(this.yksikko_input);
            this.Controls.Add(this.nimike_input);
            this.Controls.Add(this.varoitus);
            this.Controls.Add(this.toistaiseksi);
            this.Controls.Add(this.paattymispaiva);
            this.Controls.Add(this.alkamispaiva);
            this.Controls.Add(this.lisaa_nappi);
            this.Controls.Add(this.yksikko);
            this.Controls.Add(this.nimike);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.henkilo_valinta);
            this.Controls.Add(this.toimisuhteikkuna_otsikko);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "ToimisuhdeIkkuna";
            this.Text = "ToimisuhdeIkkuna";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label toimisuhteikkuna_otsikko;
        private System.Windows.Forms.ListBox henkilo_valinta;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button lisaa_nappi;
        private System.Windows.Forms.DateTimePicker alkamispaiva;
        private System.Windows.Forms.DateTimePicker paattymispaiva;
        private System.Windows.Forms.CheckBox toistaiseksi;
        private System.Windows.Forms.Label varoitus;
        private System.Windows.Forms.TextBox nimike_input;
        private System.Windows.Forms.Label nimike;
        private System.Windows.Forms.Label yksikko;
        private System.Windows.Forms.TextBox yksikko_input;
    }
}