﻿namespace YrityksenTietokanta
{
    partial class KayttoLiittyma
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(KayttoLiittyma));
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.henkilosto_nappi = new System.Windows.Forms.Button();
            this.toimisuhteet_nappi = new System.Windows.Forms.Button();
            this.alku_paneeli = new System.Windows.Forms.Panel();
            this.toimisuhteet_grid = new System.Windows.Forms.DataGridView();
            this.Nimi = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Alkamispäivä = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Päättymispäivä = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Nimike = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Yksikkö = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.henkilosto_grid = new System.Windows.Forms.DataGridView();
            this.Etunimet = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Sukunimi = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Kutsumanimi = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Henkilötunnus = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Lähiosoite = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Postinumero = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Postiosoite = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.valinta_paneeli = new System.Windows.Forms.Panel();
            this.alkusivu_nappi = new System.Windows.Forms.Button();
            this.lopetus_nappi = new System.Windows.Forms.Button();
            this.yla_paneeli = new System.Windows.Forms.Panel();
            this.ohje_nappi = new System.Windows.Forms.Button();
            this.toimisuhde_nappi = new System.Windows.Forms.Button();
            this.lisays_nappi = new System.Windows.Forms.Button();
            this.Sivun_Nimi = new System.Windows.Forms.Label();
            this.ala_paneeli = new System.Windows.Forms.Panel();
            this.valikko = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.newToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.openToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator = new System.Windows.Forms.ToolStripSeparator();
            this.saveToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.saveAsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.printToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.printPreviewToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.editToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.redoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.cutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.copyToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pasteToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator4 = new System.Windows.Forms.ToolStripSeparator();
            this.selectAllToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.azToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.azToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.zaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.lisättyToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.sukunimenMukaanToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.azToolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.zaToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.lisättyToolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.nimikkeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.azToolStripMenuItem3 = new System.Windows.Forms.ToolStripMenuItem();
            this.zaToolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.lisättyToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.alku_paneeli.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.toimisuhteet_grid)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.henkilosto_grid)).BeginInit();
            this.valinta_paneeli.SuspendLayout();
            this.yla_paneeli.SuspendLayout();
            this.valikko.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 36F);
            this.label1.ForeColor = System.Drawing.Color.Gray;
            this.label1.Location = new System.Drawing.Point(13, 12);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(251, 55);
            this.label1.TabIndex = 0;
            this.label1.Text = "Tervetuloa";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F);
            this.label2.ForeColor = System.Drawing.Color.Gray;
            this.label2.Location = new System.Drawing.Point(18, 73);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(388, 24);
            this.label2.TabIndex = 1;
            this.label2.Text = "Valitse Henkilöstö tai Toimisuhteet jatkaaksesi";
            // 
            // henkilosto_nappi
            // 
            this.henkilosto_nappi.Location = new System.Drawing.Point(12, 47);
            this.henkilosto_nappi.Name = "henkilosto_nappi";
            this.henkilosto_nappi.Size = new System.Drawing.Size(115, 28);
            this.henkilosto_nappi.TabIndex = 1;
            this.henkilosto_nappi.Text = "Henkilöstö";
            this.henkilosto_nappi.UseVisualStyleBackColor = true;
            this.henkilosto_nappi.Click += new System.EventHandler(this.henkilosto_nappi_Click);
            // 
            // toimisuhteet_nappi
            // 
            this.toimisuhteet_nappi.Location = new System.Drawing.Point(12, 81);
            this.toimisuhteet_nappi.Name = "toimisuhteet_nappi";
            this.toimisuhteet_nappi.Size = new System.Drawing.Size(115, 28);
            this.toimisuhteet_nappi.TabIndex = 2;
            this.toimisuhteet_nappi.Text = "Toimisuhteet";
            this.toimisuhteet_nappi.UseVisualStyleBackColor = true;
            this.toimisuhteet_nappi.Click += new System.EventHandler(this.toimisuhteet_nappi_Click);
            // 
            // alku_paneeli
            // 
            this.alku_paneeli.Controls.Add(this.label1);
            this.alku_paneeli.Controls.Add(this.label2);
            this.alku_paneeli.Location = new System.Drawing.Point(153, 66);
            this.alku_paneeli.Name = "alku_paneeli";
            this.alku_paneeli.Size = new System.Drawing.Size(640, 342);
            this.alku_paneeli.TabIndex = 4;
            // 
            // toimisuhteet_grid
            // 
            this.toimisuhteet_grid.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.toimisuhteet_grid.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.toimisuhteet_grid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.toimisuhteet_grid.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Nimi,
            this.Alkamispäivä,
            this.Päättymispäivä,
            this.Nimike,
            this.Yksikkö});
            this.toimisuhteet_grid.Location = new System.Drawing.Point(153, 66);
            this.toimisuhteet_grid.Name = "toimisuhteet_grid";
            this.toimisuhteet_grid.Size = new System.Drawing.Size(640, 342);
            this.toimisuhteet_grid.TabIndex = 0;
            // 
            // Nimi
            // 
            this.Nimi.HeaderText = "Nimi";
            this.Nimi.Name = "Nimi";
            // 
            // Alkamispäivä
            // 
            this.Alkamispäivä.HeaderText = "Alkamispäivä";
            this.Alkamispäivä.Name = "Alkamispäivä";
            // 
            // Päättymispäivä
            // 
            this.Päättymispäivä.HeaderText = "Päättymispäivä";
            this.Päättymispäivä.Name = "Päättymispäivä";
            // 
            // Nimike
            // 
            this.Nimike.HeaderText = "Nimike";
            this.Nimike.Name = "Nimike";
            // 
            // Yksikkö
            // 
            this.Yksikkö.HeaderText = "Yksikkö";
            this.Yksikkö.Name = "Yksikkö";
            // 
            // henkilosto_grid
            // 
            this.henkilosto_grid.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.henkilosto_grid.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.henkilosto_grid.BackgroundColor = System.Drawing.SystemColors.ControlDark;
            this.henkilosto_grid.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            this.henkilosto_grid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.henkilosto_grid.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Etunimet,
            this.Sukunimi,
            this.Kutsumanimi,
            this.Henkilötunnus,
            this.Lähiosoite,
            this.Postinumero,
            this.Postiosoite});
            this.henkilosto_grid.Location = new System.Drawing.Point(153, 66);
            this.henkilosto_grid.Name = "henkilosto_grid";
            this.henkilosto_grid.Size = new System.Drawing.Size(640, 342);
            this.henkilosto_grid.TabIndex = 2;
            this.henkilosto_grid.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.henkilosto_grid_CellContentClick);
            // 
            // Etunimet
            // 
            this.Etunimet.HeaderText = "Etunimet";
            this.Etunimet.Name = "Etunimet";
            // 
            // Sukunimi
            // 
            this.Sukunimi.HeaderText = "Sukunimi";
            this.Sukunimi.Name = "Sukunimi";
            // 
            // Kutsumanimi
            // 
            this.Kutsumanimi.HeaderText = "Kutsumanimi";
            this.Kutsumanimi.Name = "Kutsumanimi";
            // 
            // Henkilötunnus
            // 
            this.Henkilötunnus.HeaderText = "Henkilötunnus";
            this.Henkilötunnus.MinimumWidth = 4;
            this.Henkilötunnus.Name = "Henkilötunnus";
            this.Henkilötunnus.ReadOnly = true;
            // 
            // Lähiosoite
            // 
            this.Lähiosoite.HeaderText = "Lähiosoite";
            this.Lähiosoite.Name = "Lähiosoite";
            // 
            // Postinumero
            // 
            this.Postinumero.HeaderText = "Postinumero";
            this.Postinumero.Name = "Postinumero";
            // 
            // Postiosoite
            // 
            this.Postiosoite.HeaderText = "Postiosoite";
            this.Postiosoite.Name = "Postiosoite";
            // 
            // valinta_paneeli
            // 
            this.valinta_paneeli.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.valinta_paneeli.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.valinta_paneeli.Controls.Add(this.alkusivu_nappi);
            this.valinta_paneeli.Controls.Add(this.henkilosto_nappi);
            this.valinta_paneeli.Controls.Add(this.lopetus_nappi);
            this.valinta_paneeli.Controls.Add(this.toimisuhteet_nappi);
            this.valinta_paneeli.Location = new System.Drawing.Point(0, 56);
            this.valinta_paneeli.Name = "valinta_paneeli";
            this.valinta_paneeli.Size = new System.Drawing.Size(147, 360);
            this.valinta_paneeli.TabIndex = 5;
            // 
            // alkusivu_nappi
            // 
            this.alkusivu_nappi.Location = new System.Drawing.Point(11, 13);
            this.alkusivu_nappi.Name = "alkusivu_nappi";
            this.alkusivu_nappi.Size = new System.Drawing.Size(115, 28);
            this.alkusivu_nappi.TabIndex = 0;
            this.alkusivu_nappi.Text = "Alkusivu";
            this.alkusivu_nappi.UseVisualStyleBackColor = true;
            this.alkusivu_nappi.Click += new System.EventHandler(this.alkusivu_nappi_Click);
            // 
            // lopetus_nappi
            // 
            this.lopetus_nappi.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lopetus_nappi.Location = new System.Drawing.Point(12, 320);
            this.lopetus_nappi.Name = "lopetus_nappi";
            this.lopetus_nappi.Size = new System.Drawing.Size(115, 28);
            this.lopetus_nappi.TabIndex = 3;
            this.lopetus_nappi.Text = "Lopeta";
            this.lopetus_nappi.UseVisualStyleBackColor = true;
            this.lopetus_nappi.Click += new System.EventHandler(this.lopetus_nappi_Click);
            // 
            // yla_paneeli
            // 
            this.yla_paneeli.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.yla_paneeli.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.yla_paneeli.Controls.Add(this.ohje_nappi);
            this.yla_paneeli.Controls.Add(this.toimisuhde_nappi);
            this.yla_paneeli.Controls.Add(this.lisays_nappi);
            this.yla_paneeli.Controls.Add(this.Sivun_Nimi);
            this.yla_paneeli.Location = new System.Drawing.Point(0, 27);
            this.yla_paneeli.Name = "yla_paneeli";
            this.yla_paneeli.Size = new System.Drawing.Size(802, 33);
            this.yla_paneeli.TabIndex = 6;
            // 
            // ohje_nappi
            // 
            this.ohje_nappi.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.ohje_nappi.Location = new System.Drawing.Point(725, 3);
            this.ohje_nappi.Name = "ohje_nappi";
            this.ohje_nappi.Size = new System.Drawing.Size(66, 26);
            this.ohje_nappi.TabIndex = 6;
            this.ohje_nappi.Text = "Ohjeet";
            this.ohje_nappi.UseVisualStyleBackColor = true;
            // 
            // toimisuhde_nappi
            // 
            this.toimisuhde_nappi.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.toimisuhde_nappi.Location = new System.Drawing.Point(605, 3);
            this.toimisuhde_nappi.Name = "toimisuhde_nappi";
            this.toimisuhde_nappi.Size = new System.Drawing.Size(114, 26);
            this.toimisuhde_nappi.TabIndex = 5;
            this.toimisuhde_nappi.Text = "Lisää toimisuhde";
            this.toimisuhde_nappi.UseVisualStyleBackColor = true;
            this.toimisuhde_nappi.Click += new System.EventHandler(this.toimisuhde_nappi_Click);
            // 
            // lisays_nappi
            // 
            this.lisays_nappi.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lisays_nappi.Location = new System.Drawing.Point(505, 3);
            this.lisays_nappi.Name = "lisays_nappi";
            this.lisays_nappi.Size = new System.Drawing.Size(94, 26);
            this.lisays_nappi.TabIndex = 4;
            this.lisays_nappi.Text = "Lisää henkilö";
            this.lisays_nappi.UseVisualStyleBackColor = true;
            this.lisays_nappi.Click += new System.EventHandler(this.lisays_nappi_Click);
            // 
            // Sivun_Nimi
            // 
            this.Sivun_Nimi.AutoSize = true;
            this.Sivun_Nimi.BackColor = System.Drawing.SystemColors.Control;
            this.Sivun_Nimi.Font = new System.Drawing.Font("Microsoft YaHei", 15F);
            this.Sivun_Nimi.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.Sivun_Nimi.Location = new System.Drawing.Point(12, 3);
            this.Sivun_Nimi.Name = "Sivun_Nimi";
            this.Sivun_Nimi.Size = new System.Drawing.Size(77, 27);
            this.Sivun_Nimi.TabIndex = 0;
            this.Sivun_Nimi.Text = "Aloitus";
            // 
            // ala_paneeli
            // 
            this.ala_paneeli.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ala_paneeli.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.ala_paneeli.Location = new System.Drawing.Point(0, 412);
            this.ala_paneeli.Name = "ala_paneeli";
            this.ala_paneeli.Size = new System.Drawing.Size(802, 27);
            this.ala_paneeli.TabIndex = 7;
            // 
            // valikko
            // 
            this.valikko.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.editToolStripMenuItem,
            this.toolsToolStripMenuItem});
            this.valikko.Location = new System.Drawing.Point(0, 0);
            this.valikko.Name = "valikko";
            this.valikko.Size = new System.Drawing.Size(801, 24);
            this.valikko.TabIndex = 8;
            this.valikko.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.newToolStripMenuItem,
            this.openToolStripMenuItem,
            this.toolStripSeparator,
            this.saveToolStripMenuItem,
            this.saveAsToolStripMenuItem,
            this.toolStripSeparator1,
            this.printToolStripMenuItem,
            this.printPreviewToolStripMenuItem,
            this.toolStripSeparator2,
            this.exitToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(65, 20);
            this.fileToolStripMenuItem.Text = "Tiedosto";
            // 
            // newToolStripMenuItem
            // 
            this.newToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("newToolStripMenuItem.Image")));
            this.newToolStripMenuItem.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.newToolStripMenuItem.Name = "newToolStripMenuItem";
            this.newToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.N)));
            this.newToolStripMenuItem.Size = new System.Drawing.Size(146, 22);
            this.newToolStripMenuItem.Text = "New";
            this.newToolStripMenuItem.Click += new System.EventHandler(this.newToolStripMenuItem_Click);
            // 
            // openToolStripMenuItem
            // 
            this.openToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("openToolStripMenuItem.Image")));
            this.openToolStripMenuItem.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.openToolStripMenuItem.Name = "openToolStripMenuItem";
            this.openToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.O)));
            this.openToolStripMenuItem.Size = new System.Drawing.Size(146, 22);
            this.openToolStripMenuItem.Text = "&Open";
            // 
            // toolStripSeparator
            // 
            this.toolStripSeparator.Name = "toolStripSeparator";
            this.toolStripSeparator.Size = new System.Drawing.Size(143, 6);
            // 
            // saveToolStripMenuItem
            // 
            this.saveToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("saveToolStripMenuItem.Image")));
            this.saveToolStripMenuItem.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.saveToolStripMenuItem.Name = "saveToolStripMenuItem";
            this.saveToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.S)));
            this.saveToolStripMenuItem.Size = new System.Drawing.Size(146, 22);
            this.saveToolStripMenuItem.Text = "&Save";
            // 
            // saveAsToolStripMenuItem
            // 
            this.saveAsToolStripMenuItem.Name = "saveAsToolStripMenuItem";
            this.saveAsToolStripMenuItem.Size = new System.Drawing.Size(146, 22);
            this.saveAsToolStripMenuItem.Text = "Save &As";
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(143, 6);
            // 
            // printToolStripMenuItem
            // 
            this.printToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("printToolStripMenuItem.Image")));
            this.printToolStripMenuItem.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.printToolStripMenuItem.Name = "printToolStripMenuItem";
            this.printToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.P)));
            this.printToolStripMenuItem.Size = new System.Drawing.Size(146, 22);
            this.printToolStripMenuItem.Text = "&Print";
            // 
            // printPreviewToolStripMenuItem
            // 
            this.printPreviewToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("printPreviewToolStripMenuItem.Image")));
            this.printPreviewToolStripMenuItem.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.printPreviewToolStripMenuItem.Name = "printPreviewToolStripMenuItem";
            this.printPreviewToolStripMenuItem.Size = new System.Drawing.Size(146, 22);
            this.printPreviewToolStripMenuItem.Text = "Print Pre&view";
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(143, 6);
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(146, 22);
            this.exitToolStripMenuItem.Text = "Exit";
            this.exitToolStripMenuItem.Click += new System.EventHandler(this.exitToolStripMenuItem_Click);
            // 
            // editToolStripMenuItem
            // 
            this.editToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.redoToolStripMenuItem,
            this.toolStripSeparator3,
            this.cutToolStripMenuItem,
            this.copyToolStripMenuItem,
            this.pasteToolStripMenuItem,
            this.toolStripSeparator4,
            this.selectAllToolStripMenuItem});
            this.editToolStripMenuItem.Name = "editToolStripMenuItem";
            this.editToolStripMenuItem.Size = new System.Drawing.Size(68, 20);
            this.editToolStripMenuItem.Text = "Muokkaa";
            // 
            // redoToolStripMenuItem
            // 
            this.redoToolStripMenuItem.Name = "redoToolStripMenuItem";
            this.redoToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Y)));
            this.redoToolStripMenuItem.Size = new System.Drawing.Size(144, 22);
            this.redoToolStripMenuItem.Text = "&Redo";
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(141, 6);
            // 
            // cutToolStripMenuItem
            // 
            this.cutToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("cutToolStripMenuItem.Image")));
            this.cutToolStripMenuItem.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.cutToolStripMenuItem.Name = "cutToolStripMenuItem";
            this.cutToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.X)));
            this.cutToolStripMenuItem.Size = new System.Drawing.Size(144, 22);
            this.cutToolStripMenuItem.Text = "Cu&t";
            // 
            // copyToolStripMenuItem
            // 
            this.copyToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("copyToolStripMenuItem.Image")));
            this.copyToolStripMenuItem.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.copyToolStripMenuItem.Name = "copyToolStripMenuItem";
            this.copyToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.C)));
            this.copyToolStripMenuItem.Size = new System.Drawing.Size(144, 22);
            this.copyToolStripMenuItem.Text = "&Copy";
            // 
            // pasteToolStripMenuItem
            // 
            this.pasteToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("pasteToolStripMenuItem.Image")));
            this.pasteToolStripMenuItem.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.pasteToolStripMenuItem.Name = "pasteToolStripMenuItem";
            this.pasteToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.V)));
            this.pasteToolStripMenuItem.Size = new System.Drawing.Size(144, 22);
            this.pasteToolStripMenuItem.Text = "&Paste";
            // 
            // toolStripSeparator4
            // 
            this.toolStripSeparator4.Name = "toolStripSeparator4";
            this.toolStripSeparator4.Size = new System.Drawing.Size(141, 6);
            // 
            // selectAllToolStripMenuItem
            // 
            this.selectAllToolStripMenuItem.Name = "selectAllToolStripMenuItem";
            this.selectAllToolStripMenuItem.Size = new System.Drawing.Size(144, 22);
            this.selectAllToolStripMenuItem.Text = "Select &All";
            // 
            // toolsToolStripMenuItem
            // 
            this.toolsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.azToolStripMenuItem,
            this.sukunimenMukaanToolStripMenuItem,
            this.nimikkeToolStripMenuItem});
            this.toolsToolStripMenuItem.Name = "toolsToolStripMenuItem";
            this.toolsToolStripMenuItem.Size = new System.Drawing.Size(57, 20);
            this.toolsToolStripMenuItem.Text = "Järjestä";
            // 
            // azToolStripMenuItem
            // 
            this.azToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.azToolStripMenuItem1,
            this.zaToolStripMenuItem,
            this.lisättyToolStripMenuItem});
            this.azToolStripMenuItem.Name = "azToolStripMenuItem";
            this.azToolStripMenuItem.Size = new System.Drawing.Size(145, 22);
            this.azToolStripMenuItem.Text = "Kutsumanimi";
            // 
            // azToolStripMenuItem1
            // 
            this.azToolStripMenuItem1.Name = "azToolStripMenuItem1";
            this.azToolStripMenuItem1.Size = new System.Drawing.Size(105, 22);
            this.azToolStripMenuItem1.Text = "a-z";
            this.azToolStripMenuItem1.Click += new System.EventHandler(this.azToolStripMenuItem1_Click);
            // 
            // zaToolStripMenuItem
            // 
            this.zaToolStripMenuItem.Name = "zaToolStripMenuItem";
            this.zaToolStripMenuItem.Size = new System.Drawing.Size(105, 22);
            this.zaToolStripMenuItem.Text = "z-a";
            this.zaToolStripMenuItem.Click += new System.EventHandler(this.zaToolStripMenuItem_Click);
            // 
            // lisättyToolStripMenuItem
            // 
            this.lisättyToolStripMenuItem.Name = "lisättyToolStripMenuItem";
            this.lisättyToolStripMenuItem.Size = new System.Drawing.Size(105, 22);
            this.lisättyToolStripMenuItem.Text = "lisätty";
            this.lisättyToolStripMenuItem.Click += new System.EventHandler(this.lisättyToolStripMenuItem_Click);
            // 
            // sukunimenMukaanToolStripMenuItem
            // 
            this.sukunimenMukaanToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.azToolStripMenuItem2,
            this.zaToolStripMenuItem1,
            this.lisättyToolStripMenuItem2});
            this.sukunimenMukaanToolStripMenuItem.Name = "sukunimenMukaanToolStripMenuItem";
            this.sukunimenMukaanToolStripMenuItem.Size = new System.Drawing.Size(145, 22);
            this.sukunimenMukaanToolStripMenuItem.Text = "Sukunimi";
            // 
            // azToolStripMenuItem2
            // 
            this.azToolStripMenuItem2.Name = "azToolStripMenuItem2";
            this.azToolStripMenuItem2.Size = new System.Drawing.Size(105, 22);
            this.azToolStripMenuItem2.Text = "a-z";
            this.azToolStripMenuItem2.Click += new System.EventHandler(this.azToolStripMenuItem2_Click);
            // 
            // zaToolStripMenuItem1
            // 
            this.zaToolStripMenuItem1.Name = "zaToolStripMenuItem1";
            this.zaToolStripMenuItem1.Size = new System.Drawing.Size(105, 22);
            this.zaToolStripMenuItem1.Text = "z-a";
            this.zaToolStripMenuItem1.Click += new System.EventHandler(this.zaToolStripMenuItem1_Click);
            // 
            // lisättyToolStripMenuItem2
            // 
            this.lisättyToolStripMenuItem2.Name = "lisättyToolStripMenuItem2";
            this.lisättyToolStripMenuItem2.Size = new System.Drawing.Size(105, 22);
            this.lisättyToolStripMenuItem2.Text = "lisätty";
            this.lisättyToolStripMenuItem2.Click += new System.EventHandler(this.lisättyToolStripMenuItem2_Click);
            // 
            // nimikkeToolStripMenuItem
            // 
            this.nimikkeToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.azToolStripMenuItem3,
            this.zaToolStripMenuItem2,
            this.lisättyToolStripMenuItem1});
            this.nimikkeToolStripMenuItem.Name = "nimikkeToolStripMenuItem";
            this.nimikkeToolStripMenuItem.Size = new System.Drawing.Size(145, 22);
            this.nimikkeToolStripMenuItem.Text = "Nimike";
            // 
            // azToolStripMenuItem3
            // 
            this.azToolStripMenuItem3.Name = "azToolStripMenuItem3";
            this.azToolStripMenuItem3.Size = new System.Drawing.Size(105, 22);
            this.azToolStripMenuItem3.Text = "a-z";
            this.azToolStripMenuItem3.Click += new System.EventHandler(this.azToolStripMenuItem3_Click);
            // 
            // zaToolStripMenuItem2
            // 
            this.zaToolStripMenuItem2.Name = "zaToolStripMenuItem2";
            this.zaToolStripMenuItem2.Size = new System.Drawing.Size(105, 22);
            this.zaToolStripMenuItem2.Text = "z-a";
            this.zaToolStripMenuItem2.Click += new System.EventHandler(this.zaToolStripMenuItem2_Click);
            // 
            // lisättyToolStripMenuItem1
            // 
            this.lisättyToolStripMenuItem1.Name = "lisättyToolStripMenuItem1";
            this.lisättyToolStripMenuItem1.Size = new System.Drawing.Size(105, 22);
            this.lisättyToolStripMenuItem1.Text = "lisätty";
            // 
            // KayttoLiittyma
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(801, 444);
            this.Controls.Add(this.toimisuhteet_grid);
            this.Controls.Add(this.ala_paneeli);
            this.Controls.Add(this.henkilosto_grid);
            this.Controls.Add(this.yla_paneeli);
            this.Controls.Add(this.alku_paneeli);
            this.Controls.Add(this.valinta_paneeli);
            this.Controls.Add(this.valikko);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.valikko;
            this.Name = "KayttoLiittyma";
            this.Text = "Tietokanta";
            this.alku_paneeli.ResumeLayout(false);
            this.alku_paneeli.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.toimisuhteet_grid)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.henkilosto_grid)).EndInit();
            this.valinta_paneeli.ResumeLayout(false);
            this.yla_paneeli.ResumeLayout(false);
            this.yla_paneeli.PerformLayout();
            this.valikko.ResumeLayout(false);
            this.valikko.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button henkilosto_nappi;
        private System.Windows.Forms.Button toimisuhteet_nappi;
        private System.Windows.Forms.Panel alku_paneeli;
        private System.Windows.Forms.Panel valinta_paneeli;
        private System.Windows.Forms.Panel yla_paneeli;
        private System.Windows.Forms.Panel ala_paneeli;
        private System.Windows.Forms.Button alkusivu_nappi;
        private System.Windows.Forms.Label Sivun_Nimi;
        private System.Windows.Forms.Button lisays_nappi;
        private System.Windows.Forms.Button ohje_nappi;
        private System.Windows.Forms.Button lopetus_nappi;
        private System.Windows.Forms.MenuStrip valikko;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem newToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem openToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator;
        private System.Windows.Forms.ToolStripMenuItem saveToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem saveAsToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripMenuItem printToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem printPreviewToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem editToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem redoToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private System.Windows.Forms.ToolStripMenuItem cutToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem copyToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem pasteToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator4;
        private System.Windows.Forms.ToolStripMenuItem selectAllToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem toolsToolStripMenuItem;
        private System.Windows.Forms.DataGridView henkilosto_grid;
        private System.Windows.Forms.DataGridViewTextBoxColumn Etunimet;
        private System.Windows.Forms.DataGridViewTextBoxColumn Sukunimi;
        private System.Windows.Forms.DataGridViewTextBoxColumn Kutsumanimi;
        private System.Windows.Forms.DataGridViewTextBoxColumn Henkilötunnus;
        private System.Windows.Forms.DataGridViewTextBoxColumn Lähiosoite;
        private System.Windows.Forms.DataGridViewTextBoxColumn Postinumero;
        private System.Windows.Forms.DataGridViewTextBoxColumn Postiosoite;
        private System.Windows.Forms.ToolStripMenuItem azToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem azToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem zaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem sukunimenMukaanToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem azToolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem zaToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem nimikkeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem azToolStripMenuItem3;
        private System.Windows.Forms.ToolStripMenuItem zaToolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem lisättyToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem lisättyToolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem lisättyToolStripMenuItem1;
        private System.Windows.Forms.Button toimisuhde_nappi;
        private System.Windows.Forms.DataGridView toimisuhteet_grid;
        private System.Windows.Forms.DataGridViewTextBoxColumn Nimi;
        private System.Windows.Forms.DataGridViewTextBoxColumn Alkamispäivä;
        private System.Windows.Forms.DataGridViewTextBoxColumn Päättymispäivä;
        private System.Windows.Forms.DataGridViewTextBoxColumn Nimike;
        private System.Windows.Forms.DataGridViewTextBoxColumn Yksikkö;
    }
}

