﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Text.RegularExpressions;

namespace YrityksenTietokanta
{
    public partial class LisaysIkkuna : Form
    {
    // Tallennetaan textBox:in syötetyt tiedot väliaikaisesti
    // struct:in sisälle. Tiedot tallennetaan tämän kautta eteenpäin.
        protected struct LisattavaHenkilo
        {
            public string etunimet;
            public string sukunimi;
            public string kutsumanimi;
            public string henkilotunnus;
            public string lahiosoite;
            public string postinumero;
            public string postitoimipaikka;
        }
        private LisattavaHenkilo lisattavaHenkilo; // Luotu struct.

        public LisaysIkkuna()
        {
        // Alustetaan tyhjäksi, sillä kentän täyttäminen ei ole pakollista.
            this.lisattavaHenkilo.kutsumanimi = "";

            InitializeComponent();
        }

    // Funktiot varoituksen näyttämiseen lisaysIkkuna:ssa.
        private void varoita(string varoitusteksti)
        {
            varoitus_teksti.Text = varoitusteksti;
        }

    // Funktiot struct:in sisällön muuttamiseen (tiivistetyssä muodossa):
        private void etunimi_input_TextChanged(object sender, EventArgs e) {
            this.lisattavaHenkilo.etunimet = etunimi_input.Text;
        }
        private void sukunimi_input_TextChanged(object sender, EventArgs e) {
            this.lisattavaHenkilo.sukunimi = sukunimi_input.Text;
        }
        private void kutsumanimi_input_TextChanged(object sender, EventArgs e) {
            this.lisattavaHenkilo.kutsumanimi = kutsumanimi_input.Text;
        }
        private void henkilotunnus_input_TextChanged(object sender, EventArgs e) {
            this.lisattavaHenkilo.henkilotunnus = henkilotunnus_input.Text;
        }
        private void lahiosoite_input_TextChanged(object sender, EventArgs e) {
            this.lisattavaHenkilo.lahiosoite = lahiosoite_input.Text;
        }
        private void postinumero_input_TextChanged(object sender, EventArgs e) {
            this.lisattavaHenkilo.postinumero = postinumero_input.Text;
        }
        private void postitoimipaikka_input_TextChanged(object sender, EventArgs e) {
            this.lisattavaHenkilo.postitoimipaikka = postitoimipaikka_input.Text;
        }

    // Napin painallukseen funktio, joka tarkistaa ja tallentaa tiedot.
        private void hyvaksy_uusi_nappi_Click(object sender, EventArgs e)
        {
            string hetu_merkit = "0123456789ABCDEFHJKLMNPRSTUVWXY";
            string hetu_alku, hetu_loput; // <- tarkistusmerkkiä varten.
            char tarkistusmerkki = ' ';
            int hetu_numerona;

            string[] etunimet, katuosoite;
            bool kentatOvatValideja = true; // Ei tallenna, jos `false`.
            bool kutsumanimiOikea = false;

        // Merkkien tarkistukseen käytettävä funktio.
            bool merkitHyvaksyttavia(string merkkijono)
            {
                return Regex.IsMatch(merkkijono.ToLower(), @"^([a-z]|å|ä|ö)+$");
            }

        // Tarkistetaan kaikkien kenttien validisuus:
            if (  string.IsNullOrEmpty(this.lisattavaHenkilo.etunimet)
              ||  string.IsNullOrEmpty(this.lisattavaHenkilo.sukunimi)
              ||  string.IsNullOrEmpty(this.lisattavaHenkilo.lahiosoite)
              ||  string.IsNullOrEmpty(this.lisattavaHenkilo.postinumero)
              ||  string.IsNullOrEmpty(this.lisattavaHenkilo.postitoimipaikka)
            ) {
                varoita("Älä jätä kenttia tyhjiksi!\n(poislukien kutsumanimi)");
            }
            else if (this.lisattavaHenkilo.henkilotunnus.Length != 11)
            {
                varoita("Henkilötunnuksen täytyy olla\n11 merkkiä pitkä!");
            }
            else // Jatketaan yksittäisten kenttien tarkistuksilla.
            {
            // `.Split()`, kun kentät tarkistettu null ja "" varalta.
                etunimet = this.lisattavaHenkilo.etunimet.Split(' ');
                katuosoite = this.lisattavaHenkilo.lahiosoite.Split(' ');

                try
                {
                    hetu_alku = this.lisattavaHenkilo.henkilotunnus.Substring(0, 6);
                    hetu_loput = this.lisattavaHenkilo.henkilotunnus.Substring(7, 3);

                // Tallennetaan `TryParse` tulos ja tuodaan parsittu luku muuttujaan `hetu_numerona`.
                    if (!int.TryParse(hetu_alku + hetu_loput, out hetu_numerona))
                    {
                        varoita("Henkilötunnus: " + this.lisattavaHenkilo.henkilotunnus + "\n on virheellinen.");
                        kentatOvatValideja = false;
                    }

                // Tarkistettava hetun loppuosa kirjaimena:
                    tarkistusmerkki = hetu_merkit[hetu_numerona % 31];
                }
                catch(Exception)
                {
                    varoita("Henkilötunnus: "+this.lisattavaHenkilo.henkilotunnus+"\n ei ole tarpeeksi pitkä.");
                    kentatOvatValideja = false;
                }

            // Iteroidaan etunimien läpi ja tarkastetaan oikeellisuus.
                foreach (string nimi in etunimet)
                {
                    if (!merkitHyvaksyttavia(nimi))
                    {
                        varoita("Etunimi: "+nimi+"\n sisältää kiellettyjä merkkejä.");
                        kentatOvatValideja = false;
                    }
                    if (nimi == this.lisattavaHenkilo.kutsumanimi)
                    {
                        kutsumanimiOikea = true;
                    }
                }

            // Jos etunimiä oli vain yksi, ei kutsumanimeä tarvitse:.
                if (etunimet.Length == 1) {
                    this.lisattavaHenkilo.kutsumanimi = etunimet[0];
                    kutsumanimiOikea = true;
                }

            // Ruma if-kasa yksittäisten kenttien tarkistukseen.
                if (!merkitHyvaksyttavia(this.lisattavaHenkilo.sukunimi)) {
                    varoita("Sukunimi: "+this.lisattavaHenkilo.sukunimi+"\n sisältää kiellettyjä merkkejä.");
                    kentatOvatValideja = false;
                }
                if (!kutsumanimiOikea) {
                    varoita("Kutsumanimieä: "+this.lisattavaHenkilo.kutsumanimi+"\n ei löydy etunimistä.");
                    kentatOvatValideja = false;
                }
                if (!Regex.IsMatch(lisattavaHenkilo.henkilotunnus, @"^[0-9]{6}[+-A]{1}[0-9]{3}.$")) {
                    varoita("Henkkilötunnus: "+this.lisattavaHenkilo.henkilotunnus+"\n on väärän muotoinen.");
                    kentatOvatValideja = false;
                }
                if (!(lisattavaHenkilo.henkilotunnus[10] == tarkistusmerkki)) {
                    varoita("Hetun tarkistusmerkki: "+lisattavaHenkilo.henkilotunnus[10]+"\n ei ole oikein.");
                    kentatOvatValideja = false;
                }
                if (!merkitHyvaksyttavia(katuosoite[0]) || katuosoite.Length < 2) {
                    varoita("Lähiosoite: "+this.lisattavaHenkilo.lahiosoite+"\n ei ole hyväksyttävä.");
                    kentatOvatValideja = false;
                }
                if (!Regex.IsMatch(this.lisattavaHenkilo.postinumero, @"^[0-9]+$")) {
                    varoita("Postinumeron: "+this.lisattavaHenkilo.postinumero+"\n täytyy koostua numeroista.");
                    kentatOvatValideja = false;
                }
                if (!merkitHyvaksyttavia(this.lisattavaHenkilo.postitoimipaikka)) {
                    varoita("Postitoimipaikka: "+this.lisattavaHenkilo.postitoimipaikka+"\n sisältää kiellettyjä merkkejä.");
                    kentatOvatValideja = false;
                }

                if (kentatOvatValideja) // Jos kaikki ok, tallennetaan uusi henkilö:
                {
                    string aikaisemmat_tiedot = File.Exists(YrityksenTietokanta.kotipolku+"/.tietokanta/henkilosto")
                        ? YrityksenTietokanta.LueTiedot("henkilosto")
                        : ""; // Annetaan tyhjä string, jos tiedostoa ei ole.

                    string tallennettava_henkilosto = aikaisemmat_tiedot +
                        this.lisattavaHenkilo.etunimet + "%" +
                        this.lisattavaHenkilo.sukunimi + "%" +
                        this.lisattavaHenkilo.kutsumanimi + "%" +
                        this.lisattavaHenkilo.henkilotunnus + "%" +
                        this.lisattavaHenkilo.lahiosoite + "%" +
                        this.lisattavaHenkilo.postinumero + "%" +
                        this.lisattavaHenkilo.postitoimipaikka +
                        Environment.NewLine;

                    YrityksenTietokanta.TallennaTiedot(tallennettava_henkilosto, "henkilosto");
                    YrityksenTietokanta.LuoLokiRivi("Loi uuden henkilön.");
                    this.Close(); // Suljetaan ikkuna.
                }
            }
        }
    }
}
