﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace YrityksenTietokanta
{
    public partial class ToimisuhdeIkkuna : Form
    {
        private struct Toimisuhde
        {
            public string nimi;
            public DateTime alku;
            public DateTime loppu;
            public bool toistaiseksi;
            public string nimike;
            public string yksikko;
        }
        private Toimisuhde toimisuhde = new Toimisuhde();

        public ToimisuhdeIkkuna()
        {
            InitializeComponent();

            if (File.Exists(YrityksenTietokanta.kotipolku+"/.tietokanta/henkilosto"))
            {
                this.henkilo_valinta.Items.Clear();

            // Populoidaan ListBox elementtti henkilöstön nimillä.
                List<string> henkilot = YrityksenTietokanta.LueTiedot("henkilosto")
                    .Split('\n').Where(h => !string.IsNullOrEmpty(h)).Distinct().ToList();

                for (int i = 0; i < henkilot.Count; ++i)
                {
                    this.henkilo_valinta.Items.Add(henkilot[i].Split('%')[2]);
                }

            // Check-box on alustavasti ruksaamaton.
                this.toimisuhde.toistaiseksi = false;

            // `DateTime` objectit alustetan nykypäivään.
                this.toimisuhde.alku = DateTime.Now;
                this.toimisuhde.loppu = DateTime.Now;
            }
            else
            {
                varoita("Ei valittavia henkilöitä!");
            }
        }
     
    // Kutsutaan, jos kentät eivät ole oikein.
        private void varoita(string ilmoitus)
        {
            this.varoitus.Text = ilmoitus;
        }

        private void henkilo_valinta_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.toimisuhde.nimi = this.henkilo_valinta
                .GetItemText(this.henkilo_valinta.SelectedItem);
        }

    // Hoitavat `DateTimePicker` muutokset.
        private void alkamispaiva_ValueChanged(object sender, EventArgs e) {
            this.toimisuhde.alku = this.alkamispaiva.Value;
        }
        private void paattymispaiva_ValueChanged(object sender, EventArgs e) {
            this.toimisuhde.loppu = this.paattymispaiva.Value;
        }

    // Check-box muutos, kun päättymispäivä on "toistaiseksi".
        private void toistaiseksi_CheckedChanged(object sender, EventArgs e) {
            bool voimassa = !this.paattymispaiva.Enabled;
            this.toimisuhde.toistaiseksi = voimassa;
            this.paattymispaiva.Enabled = voimassa;
        }

    // Nimikkeen ja yksikon input-kenttien muutokset.
        private void nimike_input_TextChanged(object sender, EventArgs e) {
            this.toimisuhde.nimike = this.nimike_input.Text;
        }
        private void yksikko_input_TextChanged(object sender, EventArgs e) {
            this.toimisuhde.yksikko = this.yksikko_input.Text;
        }

        private void lisaa_nappi_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(this.toimisuhde.nimike)
             || string.IsNullOrEmpty(this.toimisuhde.yksikko)
            ) {
                varoita("Täytä kaikki kentät!");
            }
            else if (string.IsNullOrEmpty(this.toimisuhde.nimi))
            {
                varoita("Valitse henkilö listasta!");
            }
            else if (!this.toimisuhde.toistaiseksi
            // Jos päivät samat tai loppumisajankohta aikaisemmin kuin aloitus.
             && (DateTime.Compare(this.toimisuhde.loppu, this.toimisuhde.alku) <= 0)
            ) {
                varoita("Loppumisajankohta väärin.");
            }
            else
            {
                string tiedot =
                    this.toimisuhde.nimi + "%" +
                    this.toimisuhde.alku.ToString() + "%" +
                    this.toimisuhde.loppu.ToString() + "%" +
                    this.toimisuhde.nimike + "%" +
                    this.toimisuhde.yksikko + "%" +
                    Environment.NewLine;

                YrityksenTietokanta.TallennaTiedot(tiedot, "toimisuhteet");
                YrityksenTietokanta.LuoLokiRivi("Loi uuden toimisuhteen.");
                this.Close(); // Suljetaan ikkuna.
            }
        }
    }
}
