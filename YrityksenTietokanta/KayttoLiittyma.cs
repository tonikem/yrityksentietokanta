﻿using System;
using System.IO;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Text.RegularExpressions;

namespace YrityksenTietokanta
{
    public partial class KayttoLiittyma : Form
    {
    // getter/setter valitulle paneelille.
        public string valittu_paneeli
        {
            get { return this.Sivun_Nimi.Text; }
            set { this.Sivun_Nimi.Text = value; }
        }

    // Määrittää sorttaus-järjestyksen.
        public bool kaanteinen = false;
        public int jarjestys = 0;

        public KayttoLiittyma()
        {
            this.MinimumSize = new Size(580, 340);
            InitializeComponent();

            //this.henkilosto_grid.AutoGenerateColumns = true;

        // Varmistetaan alkunäkymä.
            MuutaNakymaa("Aloitus");

        // Ensimmäinen loki-merkintä.
            YrityksenTietokanta.LuoLokiRivi("Avasi tietokantaohjelman.");
        }

    // Ylikirjoitetaan metodi, joka käynnistyy kun formi sulkeutuu.
        protected override void OnFormClosing(FormClosingEventArgs e)
        {
            YrityksenTietokanta.LuoLokiRivi("Sulki tietokantaohjelman."+Environment.NewLine);
            base.OnFormClosing(e);
        }

    // Tämä funktio vaihtaa näkyvillä olevaa paneelia.
        public void MuutaNakymaa(string paneeli)
        {
        // Asetetaan valitun paneelin nimi otsikolle.
            this.valittu_paneeli = paneeli;

        // Piilotetaan ensin jokainen paneeli.
            toimisuhteet_grid.Visible = false;
            henkilosto_grid.Visible = false;
            alku_paneeli.Visible = false;

        // Tuodaan esiin haluttu paneeli.
            switch (paneeli)
            {
                case "Aloitus":
                    alku_paneeli.Visible = true;
                    break;

                case "Henkilöstö":
                    henkilosto_grid.Visible = true;
                    break;

                case "Toimisuhteet":
                    toimisuhteet_grid.Visible = true;
                    break;

                default:
                    alku_paneeli.Visible = true;
                    break;
            }
        }

        private void paivitaSivu(string sivu)
        {
            switch(sivu) // Päivitetään valittu sivu.
            {
                case "henkilosto":
                    this.henkilosto_grid.Rows.Clear();

                    string henkilosto = YrityksenTietokanta.LueTiedot("henkilosto");
                    List<string> henkilosto_rivit = henkilosto.Split('\n')
                        .Where(r => !string.IsNullOrWhiteSpace(r)).Distinct().ToList();

                    if (jarjestys == 1 || jarjestys == 2)
                    {
                        if (kaanteinen)
                            henkilosto_rivit.Sort((x, y) => y.Split('%')[jarjestys]
                                      .CompareTo(x.Split('%')[jarjestys])
                            );
                        else
                            henkilosto_rivit.Sort((x, y) => x.Split('%')[jarjestys]
                                      .CompareTo(y.Split('%')[jarjestys])
                            );
                    }

                    foreach (string rivi in henkilosto_rivit)
                    {
                        this.henkilosto_grid.Rows.Add(rivi.Split('%'));
                    }
                    break;

                case "toimisuhteet":
                    this.toimisuhteet_grid.Rows.Clear();

                    string toimisuhteet = YrityksenTietokanta.LueTiedot("toimisuhteet");
                    List<string> toimisuhde_rivit = toimisuhteet.Split('\n')
                        .Where(r => !string.IsNullOrWhiteSpace(r)).Distinct().ToList();

                    if (jarjestys == 3)
                    {
                        if (kaanteinen)
                            toimisuhde_rivit.Sort((x, y) => y.Split('%')[jarjestys]
                                      .CompareTo(x.Split('%')[jarjestys])
                            );
                        else
                            toimisuhde_rivit.Sort((x, y) => x.Split('%')[jarjestys]
                                      .CompareTo(y.Split('%')[jarjestys])
                            );
                    }

                    foreach (string rivi in toimisuhde_rivit)
                    {
                        this.toimisuhteet_grid.Rows.Add(rivi.Split('%'));
                    }
                    break;

                default:
                    // Mitään ei tapahdu.
                    break;
            }

        }

    // Funktiot nappien tapahtumien käsittelyyn.
        private void alkusivu_nappi_Click(object sender, EventArgs e)
        {
            MuutaNakymaa("Aloitus");
        }
        private void henkilosto_nappi_Click(object sender, EventArgs e)
        {
            MuutaNakymaa("Henkilöstö");

            if (File.Exists(YrityksenTietokanta.kotipolku + "/.tietokanta/henkilosto"))
                paivitaSivu("henkilosto");
        }
        private void toimisuhteet_nappi_Click(object sender, EventArgs e)
        {
            MuutaNakymaa("Toimisuhteet");

            if (File.Exists(YrityksenTietokanta.kotipolku + "/.tietokanta/toimisuhteet"))
                paivitaSivu("toimisuhteet");
        }

        private void lopetus_nappi_Click(object sender, EventArgs e)
        {
            LopetusIkkuna lopetusIkkuna = new LopetusIkkuna();
            lopetusIkkuna.ShowDialog(this);
        }

    // Valikkopainike applikaation sulkemiseen.
        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            LopetusIkkuna lopetusIkkuna = new LopetusIkkuna();
            lopetusIkkuna.ShowDialog(this);
        }

        private void lisays_nappi_Click(object sender, EventArgs e)
        {
            LisaysIkkuna lisaysIkkuna = new LisaysIkkuna();
            lisaysIkkuna.Show();
        }

        private void toimisuhde_nappi_Click(object sender, EventArgs e)
        {
            if (File.Exists(YrityksenTietokanta.kotipolku + "/.tietokanta/henkilosto"))
            {
                ToimisuhdeIkkuna toimisuhdeIkkuna = new ToimisuhdeIkkuna();
                toimisuhdeIkkuna.Show();
            }
            else
            {
                MessageBox.Show("Et voi lisätä toimisuhteita\nsilä kannassa ei ole henkilöstöä.",
                    "Huom!", MessageBoxButtons.OK, MessageBoxIcon.Error); // Varoitus.
            }
        }

    // Valikkopainike uuden henkilön luomiseen.
        private void newToolStripMenuItem_Click(object sender, EventArgs e)
        {
            LisaysIkkuna lisaysIkkuna = new LisaysIkkuna();
            lisaysIkkuna.Show();
        }

    // Järjestä -valikon vaihtoehdot (Kutsumanimi, Sukunimi, Nimike).
    // Jokaiseen kuuluvat [a-z] ja [z-a] vaihtoehdot listattuna tässä:
        private void azToolStripMenuItem1_Click(object sender, EventArgs e) {
            if (File.Exists(YrityksenTietokanta.kotipolku + "/.tietokanta/henkilosto")) {
                this.kaanteinen = false;
                this.jarjestys = 2;
                paivitaSivu("henkilosto");
            }
        }
        private void zaToolStripMenuItem_Click(object sender, EventArgs e) {
            if (File.Exists(YrityksenTietokanta.kotipolku + "/.tietokanta/henkilosto")) {
                this.kaanteinen = true;
                this.jarjestys = 2;
                paivitaSivu("henkilosto");
            }
        }
        private void azToolStripMenuItem2_Click(object sender, EventArgs e) {
            if (File.Exists(YrityksenTietokanta.kotipolku + "/.tietokanta/henkilosto")) {
                this.kaanteinen = false;
                this.jarjestys = 1;
                paivitaSivu("henkilosto");
            }
        }
        private void zaToolStripMenuItem1_Click(object sender, EventArgs e) {
            if (File.Exists(YrityksenTietokanta.kotipolku + "/.tietokanta/henkilosto")) {
                this.kaanteinen = true;
                this.jarjestys = 1;
                paivitaSivu("henkilosto");
            }
        }
        private void azToolStripMenuItem3_Click(object sender, EventArgs e)
        {

        }
        private void zaToolStripMenuItem2_Click(object sender, EventArgs e)
        {

        }

    // Lisäksi painikkeet järjestyksen palauttamiseksi normaaliksi.
        private void lisättyToolStripMenuItem_Click(object sender, EventArgs e) {
            if (File.Exists(YrityksenTietokanta.kotipolku + "/.tietokanta/henkilosto")) {
                this.kaanteinen = false;
                this.jarjestys = 0;
                paivitaSivu("henkilosto");
            }
        }
        private void lisättyToolStripMenuItem2_Click(object sender, EventArgs e) {
            if (File.Exists(YrityksenTietokanta.kotipolku + "/.tietokanta/henkilosto")) {
                this.kaanteinen = true;
                this.jarjestys = 0;
                paivitaSivu("henkilosto");
            }
        }

        private void henkilosto_grid_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void toimisuhteet_grid_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }
    }
}
