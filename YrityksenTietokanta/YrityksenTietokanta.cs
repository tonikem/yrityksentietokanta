﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Windows.Forms;
/**
* Kaikki tärkeimmät muuttujat/funktiot on yksinkertaisuuden
* takia keskitetty tämän luokan sisälle. Niitä kutsutaan
* muista ohjelman osista staattisesti.
*/
namespace YrityksenTietokanta
{
    static class YrityksenTietokanta
    {
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

        // Luodaan tietokantakansio, jos sitä ei ole vielä olemassa.
            if (!Directory.Exists(YrityksenTietokanta.kotipolku + "/.tietokanta"))
            {
                YrityksenTietokanta.LuoTietokanta();
            }

            Application.Run(new KayttoLiittyma());
        }


    // Muuttujat, jotka käytössä kaikille ohjelman osille:

        public static string kotipolku = Environment
            .GetFolderPath(Environment.SpecialFolder.UserProfile);


    // Funktiot, jotka käytettävissä kaikille ohjelman osille:

        public static void Virheilmoitus(string ilmoitus)
        {
            MessageBox.Show(ilmoitus, "Virhe ohjelman ajossa",
                MessageBoxButtons.OK, MessageBoxIcon.Error);
        }

        public static void LuoLokiRivi(string lokirivi)
        {
            try // Kokeillaan `IOException`:n varalta:
            {
                string kayttaja_nimi = System.Security
                    .Principal.WindowsIdentity.GetCurrent().Name;

                StreamWriter sw = new StreamWriter(
                    YrityksenTietokanta.kotipolku + "/.tietokanta/loki.txt", true
                );

                sw.Write($"({DateTime.Now}) {kayttaja_nimi}:  {lokirivi}");
                sw.Flush();  sw.Write(Environment.NewLine);
                sw.Close();
            }
            catch (IOException)
            {
                YrityksenTietokanta.Virheilmoitus(
                    "Tiedosto tai kansio on muun prosessin\n" +
                    "käytössä, eikä sitä voida käyttää. Yritä\n" +
                    "hetken päästä uudelleen."
                );
            }
            catch (Exception)
            {
                YrityksenTietokanta.Virheilmoitus(
                    "Ohjelma kohtasi odottamattoman virheen.\n" +
                    "On suositeltavaa tallentaa ja käynnistää\n" +
                    "ohjelma uudelleen."
                );
            }
        }

        public static void LuoTietokanta()
        {
            try // Kokeillaan `IOException`:n varalta:
            {
                Directory.CreateDirectory(YrityksenTietokanta.kotipolku + "/.tietokanta");
            }
            catch (IOException)
            {
                YrityksenTietokanta.Virheilmoitus(
                    "Tiedosto tai kansio on muun prosessin\n" +
                    "käytössä, eikä sitä voida käyttää. Yritä\n" +
                    "hetken päästä uudelleen."
                );
            }
            catch (Exception)
            {
                YrityksenTietokanta.Virheilmoitus(
                    "Ohjelma kohtasi odottamattoman virheen.\n" +
                    "On suositeltavaa tallentaa ja käynnistää\n" +
                    "ohjelma uudelleen."
                );
            }
        }

        public static void TallennaTiedot(string tiedot, string tiedosto)
        {
            try // Kokeillaan `IOException`:n varalta:
            {
                StreamWriter sw = new StreamWriter(
                    YrityksenTietokanta.kotipolku + "/.tietokanta/" + tiedosto, false
                );
                sw.Write( Convert.ToBase64String(
                    System.Text.Encoding.UTF8.GetBytes(tiedot))
                );
                sw.Close();
            }
            catch (IOException)
            {
                YrityksenTietokanta.Virheilmoitus(
                    "Tiedosto tai kansio on muun prosessin\n" +
                    "käytössä, eikä sitä voida käyttää. Yritä\n" +
                    "hetken päästä uudelleen."
                );
            }
            catch (Exception)
            {
                YrityksenTietokanta.Virheilmoitus(
                    "Ohjelma kohtasi odottamattoman virheen.\n" +
                    "On suositeltavaa tallentaa ja käynnistää\n" +
                    "ohjelma uudelleen."
                );
            }
        }

        public static string LueTiedot(string tiedosto)
        {
            try // Kokeillaan `IOException`:n varalta:
            {
                StreamReader sr = new StreamReader(
                    YrityksenTietokanta.kotipolku + "/.tietokanta/" + tiedosto
                );
                string tiedot = System.Text.Encoding.UTF8.GetString(
                    Convert.FromBase64String(sr.ReadToEnd())
                );
                sr.Close();

                return tiedot;
            }
            catch (IOException)
            {
                YrityksenTietokanta.Virheilmoitus(
                    "Tiedosto tai kansio on muun prosessin\n" +
                    "käytössä, eikä sitä voida käyttää. Yritä\n" +
                    "hetken päästä uudelleen."
                );
                return "";
            }
            catch (Exception)
            {
                YrityksenTietokanta.Virheilmoitus(
                    "Ohjelma kohtasi odottamattoman virheen.\n" +
                    "On suositeltavaa tallentaa ja käynnistää\n" +
                    "ohjelma uudelleen."
                );
                return "";
            }
        }
    }
}
