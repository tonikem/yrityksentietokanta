Vaihtoehto 3 (Valittu teht�v�ksi)

Tee sovellus, jolla voi pit�� yll� yrityksen henkil�st�n tietoja
ja henkil�n toimisuhteen tietoja. Huomioi sovelluksen tietoturvallisuus.

Sovelluksen t�ytyy pit�� lokia kaikista tehdyist� toiminteista (kuka teki,
mit� teki, milloin teki). Sovelluksen k�ytt�j�n ei tarvitse kirjautua sovellukseen.

Lokitiedot tallennetaan tekstitiedostoon selkokielisen�. Henkil�iden ja toimisuhteiden tiedot
tallennetaan tiedostoon, mutta niit� ei saa p��st� lukemaan ulkopuolisella tekstieditorilla.


Henkil�ist� tallennetaan tiedot:

    etunimet
    sukunimi
    kutsumanimi
    henkil�tunnus (HETU)
    kotiosoite:
        katuosoite
        postinumero
        postitoimipaikka

Toimisuhteesta tallennetaan tiedot:

    alkamisp�iv�
    p��ttymisp�iv�
		(huom. voi olla my�s toistaiseksi voimassa oleva, jolloin p��ttymisp�iv�� ei t�ytet�)
    nimike
    yksikk�
		(osasto, jossa henkil� ty�skentelee)

 

Kaikkia tietoja tulee pysty� muokkaamaan sovelluksessa ja muutokset
tulee tallentua tiedostoon. Tietoja t�ytyy pysty� lis��m��n ja poistamaan.
Tietoja poistettaessa t�ytyy k�ytt�j�lt� kysy� varmennus tietojen poistolle.

Henkil�tunnuksen tulee olla oikeanlainen. Tee sovellukseen tarkastus, joka varmentaa
sy�tetyn henkil�tunnuksen. Ohjeet varmentamiseen l�ytyv�t http://vrk.fi/henkilotunnus.

Sovellus osaa ehdottaa postitoimipaikkaa sy�tetyn postinumeron perusteella.
Esimerkiksi kirjoittamalla postinumeron alkuun merkit 70 ehdottaa sovellus
postitoimipaikaksi Kuopio. Ehdotukset luetaan aiemmin sy�tetyist� tiedoista
(eli ensimm�isell� kerralla sovelluksella ei ole tietoja ehdotuksiksi).

Sovellus listaa henkil�t ja listaa tulee pysty� j�rjest�m��n kutsumanimen, sukunimen
sek� nimikkeen perusteella nousevaan (a - z) ja laskevaan (z - a) aakkosj�rjestykseen.
